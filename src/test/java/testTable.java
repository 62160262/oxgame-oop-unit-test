/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Acer
 */
public class testTable {

    public testTable() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    /**
     *
     * @throws Exception
     */
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void testRowX1ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.setRowCol(0, 3);
        table.checkwin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRowX2ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        table.checkwin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRowX3ByX() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        table.checkwin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRowX1ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(0, 1);
        table.setRowCol(0, 2);
        table.setRowCol(0, 3);
        table.checkwin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

    @Test
    public void testRowX2ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        table.checkwin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }
    @Test
    public void testRowX3ByO() {
        Player X = new Player('X');
        Player O = new Player('O');
        Table table = new Table(X, O);
        table.switchPlayer();
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        table.checkwin();
        assertEquals(true, table.isFinish());
        assertEquals(X, table.getWinner());
    }

}
